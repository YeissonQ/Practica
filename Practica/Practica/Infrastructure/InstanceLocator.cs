﻿

namespace Practica.Infrastructure
{
    using ViewModels;

   public class InstanceLocator
    {
        #region Properties
        public MainViewModels Main
        {
            get;
            set;
        }

        #endregion

        #region Constructor
        public InstanceLocator()
        {
            Main = new MainViewModels();
        }
        #endregion


    }
}
